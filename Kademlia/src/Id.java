import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;


public class Id {
	private int id;
	
	public Id(){
		 Random rand = new Random();
		 this.id = rand.nextInt();
		 if(this.id < 0){
			 this.id = -this.id;
		 } 
	}
	
	public Id(int n){
		 id = n;
	}
	
	
	public String idToSHA1() 
    { 
		String input = ""+this.id;
        try {  
            MessageDigest md = MessageDigest.getInstance("SHA-1"); 
  
           
            byte[] messageDigest = md.digest(input.getBytes()); 
  
            
            BigInteger no = new BigInteger(1, messageDigest); 
  
            
            String hashtext = no.toString(16); 
  
            
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
  
             
            return hashtext; 
        } 
  
        
        catch (NoSuchAlgorithmException e) { 
            throw new RuntimeException(e); 
        } 
    } 
	
	
	public int getId(){
		return this.id;
	}
	/*public static void main(String[] args) {
	    //Id i = new Id();
	   	//System.out.println(i.id);
	    //System.out.println(i.idToSHA1());
		//System.out.println(1^4);
		//int a = 7;
		//System.out.println(a&(1<<31));
		
	      
	   }*/
}

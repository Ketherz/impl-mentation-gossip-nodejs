import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;
import java.util.Map;




public class ClientDemand implements Runnable{
	private Node n;
	private Map<String, List<Message>> dht;
	private Socket sock;
	private PrintWriter writer = null;
	private BufferedInputStream reader = null;

	public ClientDemand(Socket pSock, Node no,  Map<String, List<Message>> dht){
	      sock = pSock;
	      n = no;
	      this.dht = dht;
	}
	
	 //Le traitement lanc� dans un thread s�par�
	 public void run(){
	    System.err.println("Lancement du traitement de la connexion cliente");

	    boolean closeConnexion = false;
	    //tant que la connexion est active, on traite les demandes
	    while(!sock.isClosed()){
	         
	         try {
	            
	            writer = new PrintWriter(sock.getOutputStream());
	            reader = new BufferedInputStream(sock.getInputStream());
	            
	            //On attend la demande du client            
	            String response = read();
	            InetSocketAddress remote = (InetSocketAddress)sock.getRemoteSocketAddress();
	            
	            //On affiche quelques infos pour le d�buggage
	            String debug = "";
	            debug = "Thread : " + Thread.currentThread().getName() + ". ";
	            debug += "Demande de l'adresse : " + remote.getAddress().getHostAddress() +".";
	            debug += " Sur le port : " + remote.getPort() + ".\n";
	            debug += "\t -> Commande re�ue : " + response + "\n";
	            System.err.println("\n" + debug);
	            
	            //On traite la demande du client en fonction de la commande envoy�e
	            String toSend = "";
	            response = response.toUpperCase();
	            String delims = "[,]";
	            String[] tokens = response.split(delims);
	            
	            switch(tokens[0]){
	               case "FIND":
	            	  int idGiven = Integer.parseInt(tokens[1]);
	            	  Id[] tab = this.n.find(idGiven);
	                  toSend = "";
	                  for(int i = 0; i < tab.length; i++){
	                	  if(i < tab.length-1){
	                		  toSend = toSend+(tab[i].getId())+",";
	                	  }
	                	  else{
	                		  toSend = toSend+(tab[i].getId());
	                	  }
	                  }
	                  break;
	                  
	               case "PING":
	            	   toSend = "true";
	            	   break;
	            	   
	            	   
	               case "GET":
	            	   int idGiven1 = Integer.parseInt(tokens[1]);
	            	   Id id = new Id(idGiven1);
	            	   String key = id.idToSHA1();
	            	   List<Message> list = dht.get(key);
	            	   for(int i = 0; i < list.size(); i++){
		                	  if(i < list.size()-1){
		                		  toSend = toSend+(list.get(i).getMessage())+",";
		                	  }
		                	  else{
		                		  toSend = toSend+(list.get(i).getMessage());
		                	  }
		                  }
	            	   break;
	                  
	               case "CLOSE":
	                   toSend = "Communication termin�e"; 
	                   closeConnexion = true;
	                   break;
	                   
	               default : 
	                  toSend = "Commande inconnu !";                     
	                  break;
	            }
	            
	          //On envoie la r�ponse au client
	            writer.write(toSend);
	            //Il FAUT IMPERATIVEMENT UTILISER flush()
	            //Sinon les donn�es ne seront pas transmises au client
	            //et il attendra ind�finiment
	            writer.flush();
	            
	            if(closeConnexion){
	               System.err.println("COMMANDE CLOSE DETECTEE ! ");
	               writer = null;
	               reader = null;
	               sock.close();
	               break;
	            }
	         }catch(SocketException e){
	        	 System.err.println("LA CONNEXION A ETE INTERROMPUE ! ");
	        	 break;
             } catch (IOException e) {
               e.printStackTrace();
         }
	    }
	   }
	 
	 
	//La m�thode que nous utilisons pour lire les r�ponses
	   private String read() throws IOException{      
	      String response = "";
	      int stream;
	      byte[] b = new byte[4096];
	      stream = reader.read(b);
	      response = new String(b, 0, stream);
	      return response;
	   }
}

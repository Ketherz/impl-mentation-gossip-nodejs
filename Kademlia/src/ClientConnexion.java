import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

public class ClientConnexion implements Runnable{

   private Socket connexion = null;
   private PrintWriter writer = null;
   private BufferedInputStream reader = null;
   private String response = null;
   private String command;
   private static int count = 0;
   private String name = "Client-";   
   
   public ClientConnexion(String host, int port, String c){
	  command = c;
      name += ++count;
      try {
         connexion = new Socket(host, port);
      } catch (UnknownHostException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   
   public void run(){
	   String delim = "[,]";
	   String [] tokenCommand = this.command.split(delim);
	   
	   //if(tokenCommand[0].equals("PING")){
		   /*try {
	            Thread.currentThread().sleep(1000);
	         } catch (InterruptedException e) {
	            e.printStackTrace();
	         }*/
	         try {

	            
	            writer = new PrintWriter(connexion.getOutputStream(), true);
	            reader = new BufferedInputStream(connexion.getInputStream());
	            //On envoie la commande au serveur
	            
	            
	            writer.write(command);
	            //TOUJOURS UTILISER flush() POUR ENVOYER R�ELLEMENT DES INFOS AU SERVEUR
	            writer.flush();  
	            
	            System.out.println("Commande " + command + " envoy�e au serveur");
	            
	            //On attend la r�ponse
	            this.response = read();
	            System.out.println("\t * " + name + " : R�ponse re�ue " + response);
	            
	            
	            
	         } catch (IOException e1) {
	            e1.printStackTrace();
	         }
	         
	        /* try {
	            Thread.currentThread().sleep(1000);
	         } catch (InterruptedException e) {
	            e.printStackTrace();
	         }*/
	      
	      
	      writer.write("CLOSE");
	      writer.flush();
	      writer.close();
	   }
	   
   //}
   
   public String getResponse(){
	   
	   return this.response;
   }
   
   //M�thode pour lire les r�ponses du serveur
   private String read() throws IOException{      
      String response = "";
      int stream;
      byte[] b = new byte[4096];
      stream = reader.read(b);
      response = new String(b, 0, stream);      
      return response;
   }   
}
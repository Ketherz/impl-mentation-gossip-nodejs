

public class Node{
	private int k = 8;
	private Id nodeId;
    private Id [][] tableRoutage;
    
    public Node(){
    	this.nodeId = new Id();
    	this.tableRoutage = new Id[32][k];
    }
    
    public int getK(){
    	return k;
    }
    
    public Id getNodeId(){
    	return this.nodeId;
    }
    
    public int distance(int n){
    	return this.nodeId.getId() ^ n;
    }
    
    public void insert(Id n){
    	int nId = n.getId();
    	//boolean inserted = false;
    	int i = 0;
    	while(i < this.tableRoutage.length){
    		// si c'est le bucket correspondant
    		if((nId&(1<<this.tableRoutage.length-1-i)) != (this.nodeId.getId()&(1<<this.tableRoutage.length-1-i))){
    			//si il n'est pas plein
    			if(this.estPlein(i) != -1){
    				this.tableRoutage[i][this.estPlein(i)] = n;
    			}
    			//si il est plein
    			else{
    				
    			}
    		}
    	}
    	
    	
    }
    
    public Id [] find(int n){
    	int i = 0;
    	while(i < this.tableRoutage.length){
    		// si c'est le bucket correspondant
    		if((n&(1<<this.tableRoutage.length-1-i)) != (this.nodeId.getId()&(1<<this.tableRoutage.length-1-i))){
    			return this.tableRoutage[i];
    		}
    	}
    	
    	return new Id[k];
    }
    
    private int estPlein( int l){
    	for(int i = 0; i < this.tableRoutage[l].length; i++){
    		if(this.tableRoutage[l][i].equals(null)){
    			return i;
    		}
    	}
    	return -1;
    }
	
}

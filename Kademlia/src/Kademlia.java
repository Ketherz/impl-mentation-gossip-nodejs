import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.net.Socket;
import java.io.BufferedInputStream;


public class Kademlia {
	private Node myNode;
	private Map<String, List<Message>> myDht;
	private InetAddress ip;
	private int port;
	private ServerSocket server = null;
	private boolean isRunning = true;
	private ClientConnexion connexionClient;
	
	public Node getMyNode() {
		return myNode;
	}

	public Map<String, List<Message>> getMyDht() {
		return myDht;
	}

	public InetAddress getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public Kademlia(int p){
		port = p;
		myNode = new Node();
		myDht = new HashMap<String, List<Message>>();
		try {
			ip = InetAddress.getLocalHost(); 
			
	      } catch (UnknownHostException e) {
	         e.printStackTrace();
	      }
		
		try {
            server = new ServerSocket(port, 100);
         } catch (IOException e) {
            System.err.println("Le port " + port + " est d�j� utilis� ! ");
         }
		
		
		//Toujours dans un thread � part vu qu'il est dans une boucle infinie
	      Thread t = new Thread(new Runnable(){
	         public void run(){
	            while(isRunning == true){
	               
	               try {
	                  //On attend une connexion d'un client
	                  Socket client = server.accept();
	                  
	                  //Une fois re�ue, on la traite dans un thread s�par�
	                  System.out.println("Connexion cliente re�ue.");                  
	                  Thread t = new Thread(new ClientDemand(client, myNode, myDht));
	                  t.start();
	                  
	               } catch (IOException e) {
	                  e.printStackTrace();
	               }
	            }
	            
	            try {
	               server.close();
	            } catch (IOException e) {
	               e.printStackTrace();
	               server = null;
	            }
	         }
	      });
	      
	      t.start();
		
	}
	
	public void close(){
	      isRunning = false;
	   }  
	
	
	
	
	
	
	private void bootstrap(){
		
	}
	
	public List<Id> find(Id idToFind, String ipServer, int portServer ) throws InterruptedException{
		ArrayList<Id> result = new ArrayList<Id>();
		Thread t = new Thread(this.connexionClient = new ClientConnexion(ipServer, port, "FIND,"+idToFind.getId()));
		t.start();
		t.join();
		String delims = "[,]";
        String[] tokens = this.connexionClient.getResponse().split(delims);
        for(String s : tokens){
        	result.add(new Id(Integer.parseInt(s)));
        }
		return result;
	}
	
	public List<Message> get(String key, String ipServer, int portServer ) throws InterruptedException{
		ArrayList<Message> result = new ArrayList<Message>();
		Thread t = new Thread(this.connexionClient = new ClientConnexion(ipServer, portServer, "GET,"+key));
		t.start();
		t.join();
		String delims = "[,]";
        String[] tokens = this.connexionClient.getResponse().split(delims);
        for(String s : tokens){
        	result.add(new Message(new Id(), s));
        }
		return result;
	}
	
	public boolean ping(String ipServer, int portServer) throws InterruptedException{
		boolean result = false;
		Thread t = new Thread(this.connexionClient = new ClientConnexion(ipServer, portServer, "PING"));
		t.start();
		t.join();
		String res = this.connexionClient.getResponse();
		if(res.equals("true")){
			return true;
		}
		return result;
		
	}
	
	public void put(String key, Message message){
		if(this.myDht.containsKey(key)){
			List<Message> tmp = this.myDht.get(key);
			this.myDht.put(key, tmp);
		}
		else{
			List<Message> l = new ArrayList<Message>();
			l.add(message);
			this.myDht.put(key, l);
		}
	}
	
	
	public static void main(String[] args) {
		Kademlia k1 = new Kademlia(131);
		System.out.println("K1 initialis�");
		Kademlia k2 = new Kademlia(133);
		System.out.println("K2 initialis�");
		try {
			System.out.println(k2.ping("127.0.0.1", 131));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			System.out.println(k2.find(new Id(),"127.0.0.1", 131));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	      
	      
	}
}

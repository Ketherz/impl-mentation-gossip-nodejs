
public class Message {
	private String message;
	private Id id;
	public Message(Id i, String m){
		this.message = m;
		this.id = i;
	}
	public String getMessage() {
		return message;
	}
	public Id getId() {
		return id;
	}
}

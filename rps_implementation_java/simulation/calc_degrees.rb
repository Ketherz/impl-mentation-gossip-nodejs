nodes_directory = "results"

in_degrees = Hash.new
out_degrees = Hash.new

Dir.chdir(nodes_directory)
all_nodes = Dir.entries('.').select{ |filename| filename !~ /\D/ }

all_nodes.each do |node|
	lines = IO.readlines(node)
	last_partial_view = lines.last.chomp("\n").split(' ')
	out_degrees[node] = last_partial_view.size
	last_partial_view.each do |out_node|
		if in_degrees[out_node]
			in_degrees[out_node] += 1
		else
			in_degrees[out_node] = 1
		end
	end
end

puts "Node\tOut\tIn"
all_nodes.each do |node|
	puts "#{node}\t#{out_degrees[node]}\t#{in_degrees[node]}"
end

puts "Nodes : #{all_nodes.length}"
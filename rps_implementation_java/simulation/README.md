## Simulation of an RPS implementation
The runnable JAR rps.jar comes from the source code nextdoor.
The code is based on the algorithm from the paper "Gossip-Based Peer Sampling" from M. Jelasity, S. Voulgaris, R. Guerraoui, A-M. Kermarrec, M. Van Steen (2007).

This simulation runs on localhost only.
One node instance is represented by one running jar instance. Each instance has its own local port number assigned, which enables the nodes to communicate with each other.

Several parameters of the RPS algorithm are configurable :

-> name (shorcut)
- swap (s)
- healing (h)
- view size (c)
- sleep duration between to 'active' iterations, in milliseconds (t)

Custom parameters :
- how long the node will be active, in seconds (tps)
- first node to be put up (f)

## Running a simulation (Java 11 required)
The .run_peers script sets up the first node and then the others ($nb_nodes).
All these nodes share the same parameters defined in the $options variable of the script.

1. Set the values of run_peers.sh (options and number of nodes) as desired
2. Run ./run_peers.sh

The parameters will be saved at results/parameters.
The partial views of each node will be saved as one file per node in results/.
The simulation takes *roughly* tps + 30 seconds to run (tps as set before running, then 30 additional seconds which is the timeout duration of each node).

## Print information about the nodes' in/out degrees
Run ruby calc_degrees.rb

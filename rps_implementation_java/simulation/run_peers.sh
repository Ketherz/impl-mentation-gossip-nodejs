#! /bin/bash

# [default]
# s : swap [2]
# h : healing [1]
# c : view size [6]
# t : sleep between two communications (ms) [1000]
# tps : node duration (seconds) [30]
# f : in first place if this is the first node to be put up [script takes care of this]

options='s 3 h 6 c 10 t 500 tps 20'
nb_nodes=20

mkdir results
echo $options >> results/parameters

nohup java -jar rps.jar f $options &

sleep 2

for i in $( eval echo {1..$nb_nodes} )
do
	nohup java -jar rps.jar $options &
done

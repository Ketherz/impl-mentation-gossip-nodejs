package impl;

import java.util.LinkedList;
import java.util.List;

public class Mailbox {
	
	private List<MailboxEntry> queue;
	
	public Mailbox() {
		queue = new LinkedList<MailboxEntry>();
	}
	
	public void addToQueue(MailboxEntry entry) {
		queue.add(entry);
	}
	
	public boolean hasNewMessage() {
		return !queue.isEmpty();
	}
	
	// only call this after hasNewMessage returned true
	public MailboxEntry getNextMessage() {
		if(hasNewMessage()) {
			return queue.remove(0);
		}
		return null;
	}
	
	public List<MailboxEntry> getMessageList() {
		return queue;
	}

}

package impl;

import java.io.Serializable;
import java.util.List;

public class MailboxEntry implements Serializable {
	
	private Address senderAddress;
	private List<NodeDescriptor> buffer;
	private Network.Flag flag;
	
	public MailboxEntry(Address senderAddress, List<NodeDescriptor> buffer, Network.Flag flag) {
		this.senderAddress = senderAddress;
		this.buffer = buffer;
		this.flag = flag;
	}
	
	public Address getSenderAddress() {
		return this.senderAddress;
	}
	
	public List<NodeDescriptor> getBuffer() {
		return this.buffer;
	}
	
	public Network.Flag getFlag() {
		return this.flag;
	}
}
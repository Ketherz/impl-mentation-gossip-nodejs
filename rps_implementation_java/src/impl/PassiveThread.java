package impl;

import java.io.PrintWriter;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class PassiveThread extends Thread {

	Node node;
	AtomicBoolean stop;

	public PassiveThread(Node node, AtomicBoolean stop) {
		this.node = node;
		this.stop = stop;
	}

	@Override
	public void run() {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("results/" + "passive" + node.getAddress().toString(), "UTF-8");
			while (!this.stop.get()) {
				Optional<MailboxEntry> entry = node.answerMessage();
				if(entry.isPresent()) {
					writer.println("Got message from " + entry.get().getSenderAddress());
					writer.println(getBufferString(entry.get().getBuffer()));
				}
				Thread.sleep(100); // 0.1 sec
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writer.close();
		}
	}
	
	public String getBufferString(List<NodeDescriptor> buffer) {
		String bufferString = "Contains :";
		for(NodeDescriptor nodeDesc : buffer) {
			bufferString += ", " + nodeDesc.toString();
		}
		return bufferString;
	}
}

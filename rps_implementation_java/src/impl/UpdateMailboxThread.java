package impl;

import java.util.concurrent.atomic.AtomicBoolean;

public class UpdateMailboxThread extends Thread {

	AtomicBoolean stop;

	public UpdateMailboxThread(AtomicBoolean stop) {
		this.stop = stop;
	}

	@Override
	public void run() {
		while (!this.stop.get()) {
			try {
				Network.updateInbox();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}

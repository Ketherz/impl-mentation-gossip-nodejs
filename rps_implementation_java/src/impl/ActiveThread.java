package impl;

import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicBoolean;

public class ActiveThread extends Thread {

	Node node;
	AtomicBoolean stop;

	public ActiveThread(Node node, AtomicBoolean stop) {
		this.node = node;
		this.stop = stop;
	}

	@Override
	public void run() {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("results/" + node.getAddress().toString(), "UTF-8");
			while(!this.stop.get()) {
				writer.println("Partial view before communication is : ");
				writer.println(node.getWholeViewString());
				NodeDescriptor neighb = node.communicateWithRandomNeighbour();
				Thread.sleep(Parameters.T); // T sec
				if(neighb != null) {
					writer.println("Initiated communication with " + neighb.toString());
				}
				writer.println("Partial view after communication (?) is : ");
				writer.println(node.toFileString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writer.close();
		}
	}
}

package impl;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Address implements Serializable {
	private InetAddress IP;
	private int port;
	
	public Address(InetAddress IP, int port) {
		this.IP = IP;
		this.port = port;
	}
	
	public Address(int port) {
		this.IP = InetAddress.getLoopbackAddress();
		this.port = port;
	}
	
	public InetAddress getIP() {
		return IP;
	}
	
	public int getPort() {
		return port;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
	      if(obj == this) return true;
	      if(!(obj instanceof Address)) return false;
	      Address o = (Address) obj;
	      return o.IP.equals(this.IP) &&
	        o.port == this.port;
	}
	
	@Override
	public String toString() {
		return String.valueOf(this.port);
	}
}
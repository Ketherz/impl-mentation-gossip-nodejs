package impl;

import java.io.Serializable;

public class NodeDescriptor implements Serializable {

	private Address nodeAddress;
	private int age;

	public NodeDescriptor(Address nodeAddress, int age) {
		this.nodeAddress = nodeAddress;
		this.age = age;
	}

	public Address getAddress() {
		return nodeAddress;
	}

	public int getAge() {
		return age;
	}
	
	@Override
	public String toString() {
		return "(" + String.valueOf(this.nodeAddress.getPort()) + ", " + age + ")";
	}
	
	public String toFileString() {
		return String.valueOf(this.nodeAddress.getPort());
	}

	public void increaseAgeByOne() {
		age++;
	}

	public boolean sameNodeAs(NodeDescriptor otherDescriptor) {
		return nodeAddress.equals(otherDescriptor.nodeAddress);
	}

	public boolean equals(NodeDescriptor otherDescriptor) {
		return this.nodeAddress.equals(otherDescriptor.nodeAddress) && this.age == otherDescriptor.age;
	}
}
package impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

import impl.Network.Flag;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class Node {

	private Address address;
	private Semaphore partialViewSemaphore = new Semaphore(1);
	private Semaphore inViewButNotReturnedYetSemaphore = new Semaphore(1);
	private List<NodeDescriptor> partialView;
	private List<NodeDescriptor> inViewButNotReturnedYet;

	public Node(Address address) {
		this.address = address;
		partialView = new LinkedList<>();
		inViewButNotReturnedYet = new LinkedList<>();
	}

	// for testing only
	public Node(Address address, List<NodeDescriptor> desc) {
		this.address = address;
		partialView = desc;
	}

	// for testing only
	public List<NodeDescriptor> getDescriptors() {
		return partialView;
	}

	// for testing only
	public List<NodeDescriptor> getNotReturnedYet() {
		return inViewButNotReturnedYet;
	}

	@Override
	public String toString() {
		String result = "Node " + this.address + ", table size " + this.partialView.size();
		try {
			partialViewSemaphore.acquire();
			for (NodeDescriptor nd : partialView) {
				result += "\n" + nd.toString();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			partialViewSemaphore.release();
		}
		return result;
	}
	
	public String getWholeViewString() {
		String result = "";
		try {
			partialViewSemaphore.acquire();
			for (NodeDescriptor nd : partialView) {
				result += " (" + nd.toFileString() + ", " + nd.getAge() + ")";
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			partialViewSemaphore.release();
		}
		return result;
	}
	
	public String toFileString() {
		String result = "";
		try {
			partialViewSemaphore.acquire();
			for (NodeDescriptor nd : partialView) {
				result += " " + nd.toFileString();
			}
			result = result.replaceFirst(" ", "");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			partialViewSemaphore.release();
		}
		return result;
	}

	public Address getAddress() {
		return this.address;
	}

	public NodeDescriptor getOwnNodeDescriptor() {
		return new NodeDescriptor(address, 0);
	}

	public void increaseViewAge() {
		for (NodeDescriptor nd : partialView) {
			nd.increaseAgeByOne();
		}
	}

	public void moveOldestPartialViewElementsToEnd(int k) {
		List<NodeDescriptor> sortedPartialView = new LinkedList<>(partialView);
		sortedPartialView.sort(Comparator.comparing(NodeDescriptor::getAge)); // sort by age

		int startIndexOldestElements = partialView.size() - k;
		if (startIndexOldestElements < 0) {
			startIndexOldestElements = 0;
		}
		partialView.removeAll(sortedPartialView.subList(startIndexOldestElements, sortedPartialView.size()));
		partialView.addAll(sortedPartialView.subList(startIndexOldestElements, sortedPartialView.size())); // add to
																											// end
	}

	public List<NodeDescriptor> duplicatePartialView() {
		return partialView.stream().map(e -> new NodeDescriptor(e.getAddress(), e.getAge()))
				.collect(Collectors.toCollection(LinkedList::new));
	}

	public void addToEndOfViewWithoutDuplicates(NodeDescriptor nd) {
		if (nd.sameNodeAs(getOwnNodeDescriptor())) {
			return;
		}
		Iterator<NodeDescriptor> it = partialView.listIterator();
		while (it.hasNext()) {
			NodeDescriptor viewNd = it.next();
			if (viewNd.sameNodeAs(nd)) {
				if (viewNd.getAge() > nd.getAge()) {
					it.remove();
					inViewButNotReturnedYet.remove(viewNd);
					partialView.add(nd);
					inViewButNotReturnedYet.add(nd);
				} // we assume there already was only one instance of the same
					// node in the original partialView
				return;
			}
		}
		partialView.add(nd);
		inViewButNotReturnedYet.add(nd);
	}

	public void deleteOldestElements(int k, int c) {
		List<NodeDescriptor> sortedPartialView = new LinkedList<>(partialView);
		sortedPartialView.sort(Comparator.comparing(NodeDescriptor::getAge));

		int startIndexOldestElements = partialView.size() - k;
		if (startIndexOldestElements < c) {
			startIndexOldestElements = c;
		}
		startIndexOldestElements = Integer.min(startIndexOldestElements, sortedPartialView.size());
		partialView.removeAll(sortedPartialView.subList(startIndexOldestElements, sortedPartialView.size()));
		inViewButNotReturnedYet
				.removeAll(sortedPartialView.subList(startIndexOldestElements, sortedPartialView.size()));
	}

	public void deleteFirstElements(int k, int c) {
		for (int i = 0; i < k && partialView.size() > c; i++) {
			NodeDescriptor node = partialView.remove(0);
			inViewButNotReturnedYet.remove(node);
		}
	}

	public void deleteElementsAtRandom(int c) {
		Random rand = new Random();
		while (partialView.size() > c) {
			NodeDescriptor node = partialView.remove(rand.nextInt(partialView.size()));
			inViewButNotReturnedYet.remove(node);
		}
	}

	public void viewSelect(int c, int h, int s, List<NodeDescriptor> buffer) {
		for (NodeDescriptor nd : buffer) {
			addToEndOfViewWithoutDuplicates(nd);
		}
		deleteOldestElements(h, c);
		deleteFirstElements(s, c);
		deleteElementsAtRandom(c);
	}

	public NodeDescriptor selectPeer() {
		if (partialView.size() < 1) {
			return null;
		}
		Random rand = new Random();
		int selectedPeerIndex = rand.nextInt(partialView.size());
		return partialView.get(selectedPeerIndex);
	}

	// quality of service : this method should not return the same element twice
	// while said element is in the view
	// (if possible)
	public Address getPeer() {
		try {
			inViewButNotReturnedYetSemaphore.acquire();
			if (!inViewButNotReturnedYet.isEmpty()) {
				return inViewButNotReturnedYet.remove(0).getAddress();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			inViewButNotReturnedYetSemaphore.release();
		}

		try {
			partialViewSemaphore.acquire();
			if (!partialView.isEmpty()) {
				Random rand = new Random();
				return partialView.get(rand.nextInt(partialView.size())).getAddress();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			partialViewSemaphore.release();
		}
		return null; // or address of the node that is always there ?
	}

	public NodeDescriptor communicateWithRandomNeighbour() {
		try {
			partialViewSemaphore.acquire();
			inViewButNotReturnedYetSemaphore.acquire();
			NodeDescriptor selectedPeer = this.selectPeer();
			if (selectedPeer != null) {
				initiateCommunication(selectedPeer.getAddress());
				return selectedPeer;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			partialViewSemaphore.release();
			inViewButNotReturnedYetSemaphore.release();
		}
		return null;
	}

	public void initiateCommunication(Address targetPeer) {
		List<NodeDescriptor> buffer = new LinkedList<>();
		if (Parameters.PUSH) {
			Collections.shuffle(partialView);
			moveOldestPartialViewElementsToEnd(Parameters.H);

			buffer.add(getOwnNodeDescriptor());
			List<NodeDescriptor> duplicates = duplicatePartialView();
			buffer.addAll(duplicates.subList(0, Integer.min(duplicates.size(), (Parameters.c / 2) - 1)));
		}
		try {
			Network.send(targetPeer, buffer, Flag.ASK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		increaseViewAge();
	}

	public Optional<MailboxEntry> answerMessage() {
		Optional<MailboxEntry> next = Network.retrieveNextMessage(); 
		if (next.isPresent()) {
			MailboxEntry entry = next.get();
			try {
				partialViewSemaphore.acquire();
				inViewButNotReturnedYetSemaphore.acquire();
				if (entry.getFlag() == Flag.ASK) {
					answerPush(entry.getSenderAddress(), entry.getBuffer());
				} else if (entry.getFlag() == Flag.ANSWER) {
					useAnswer(entry.getBuffer());
				}
				increaseViewAge();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				partialViewSemaphore.release();
				inViewButNotReturnedYetSemaphore.release();
			}
		}
		return next;
	}

	public void answerPush(Address address, List<NodeDescriptor> receivedBuffer) {
		List<NodeDescriptor> buffer = new LinkedList<>();
		if (Parameters.PULL) {
			buffer.add(getOwnNodeDescriptor());
			Collections.shuffle(partialView);
			moveOldestPartialViewElementsToEnd(Parameters.H);
			List<NodeDescriptor> duplicates = duplicatePartialView();
			buffer.addAll(duplicates.subList(0, Integer.min(duplicates.size(), (Parameters.c / 2) - 1)));
			try {
				Network.send(address, buffer, Flag.ANSWER);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		viewSelect(Parameters.c, Parameters.H, Parameters.S, receivedBuffer);
	}

	public void useAnswer(List<NodeDescriptor> receivedBuffer) {
		if (Parameters.PULL) {
			viewSelect(Parameters.c, Parameters.H, Parameters.S, receivedBuffer);
		}
	}

}

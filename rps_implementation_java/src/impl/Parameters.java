package impl;

public class Parameters {
	
	public static final Address alwaysThere = new Address(8622);
	
	public static int T = 1000;
	public static int c = 6;
	public static int H = 1;
	public static int S = 2;
	public static final boolean PUSH = true;
	public static final boolean PULL = true;

}

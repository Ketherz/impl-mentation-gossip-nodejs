package impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Semaphore;


public class Network {

	private static ServerSocket socket = null;
	private static Semaphore mailboxSemaphore = new Semaphore(1);
	private static Mailbox mailbox = new Mailbox();
	private static Address myAddress = null;

	public enum Flag {
		ASK, ANSWER
	};
	
	public static Address getOwnAddress() {
		return myAddress;
	}

	public static void setupSocket() throws IOException {
		socket = new ServerSocket(0);
		socket.setSoTimeout(10000);
		myAddress = new Address(socket.getInetAddress(), socket.getLocalPort());
	}

	public static void setupFirstNodeSocket() throws IOException {
		socket = new ServerSocket(Parameters.alwaysThere.getPort());
		socket.setSoTimeout(30000);
		myAddress = new Address(socket.getInetAddress(), socket.getLocalPort());
	}
	
	public static void closeSocket() throws IOException {
		if(socket != null) {
			socket.close();
		}
	}

	public static void send(Address receiverAddress, List<NodeDescriptor> buffer, Flag flag) throws UnknownHostException, IOException {
		Socket socketToReceiver = new Socket(receiverAddress.getIP(), receiverAddress.getPort());
		ObjectOutputStream out = new ObjectOutputStream(socketToReceiver.getOutputStream());
		MailboxEntry entry = new MailboxEntry(myAddress, buffer, flag);
		out.writeObject(entry);
		socketToReceiver.close();
	}

	// only call this method after hasNewMessage returned true
	public static Optional<MailboxEntry> retrieveNextMessage() {
		Optional<MailboxEntry> result = Optional.empty();
		try {
			mailboxSemaphore.acquire();
			if(mailbox.hasNewMessage()) {
				result = Optional.of(mailbox.getNextMessage());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			mailboxSemaphore.release();
		}
		return result;
	}
	
	public static void updateInbox() throws IOException, ClassNotFoundException {
		Socket socketToSender = socket.accept();
		ObjectInputStream in = new ObjectInputStream(socketToSender.getInputStream());
		MailboxEntry entry = (MailboxEntry) in.readObject();
		try {
			mailboxSemaphore.acquire();
			mailbox.addToQueue(entry);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			mailboxSemaphore.release();
		}
	}

}

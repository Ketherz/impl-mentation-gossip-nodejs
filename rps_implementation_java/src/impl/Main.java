package impl;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class Main {
	
	public static int getArgsValue(List<String> arguments, String parameter) {
		if(arguments.contains(parameter) && arguments.size() > arguments.indexOf(parameter) + 1) {
			return Integer.parseInt(arguments.get(arguments.indexOf(parameter) + 1));
		}
		return -1;
	}

	public static void main(String[] args) throws IOException {
		
		List<String> arguments = List.of(args);
		boolean isFirst = args.length > 0 && args[0].equals("f");

		int sValue = getArgsValue(arguments, "s");
		Parameters.S = (sValue > -1) ? sValue : Parameters.S;
		
		int tValue = getArgsValue(arguments, "t");
		Parameters.T = (tValue > -1) ? tValue : Parameters.T;
		
		int hValue = getArgsValue(arguments, "h");
		Parameters.H = (hValue > -1) ? hValue : Parameters.H;
		
		int cValue = getArgsValue(arguments, "c");
		Parameters.c = (cValue > -1) ? cValue : Parameters.c;
		
		int tpsValue = getArgsValue(arguments, "tps");
		int tps = (tpsValue > -1) ? tpsValue * 1000 : 30000;
		
		if(isFirst) {
			
			Node firstNode = new Node(Parameters.alwaysThere);
			try {
				Network.setupFirstNodeSocket();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
			AtomicBoolean stop = new AtomicBoolean(false);
			Thread active = new ActiveThread(firstNode, stop);
			active.start();
			Thread mailboxUpdater = new UpdateMailboxThread(stop);
			mailboxUpdater.start();
			Thread passive = new PassiveThread(firstNode, stop);
			passive.start();
			try {
				Thread.sleep(tps);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			stop.set(true);
			passive.interrupt();
			
			
		} else {
			
			try {
				Network.setupSocket();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
			Node newNode = new Node(Network.getOwnAddress());
			ServiceInstance newInstance = new ServiceInstance(newNode);
			try {
				newInstance.init(tps);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		Network.closeSocket();
				
	}

}

package impl;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServiceInstance {

	public Node node;
	private boolean initialised = false;

	public ServiceInstance(Node node) {
		this.node = node;
	}

	/**
	 * From Gossip-Based Peer Sampling paper (2007) : Initializes the service on
	 * the node if this has not been done before.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public void init(int tps) throws IOException, InterruptedException {
		node.initiateCommunication(Parameters.alwaysThere);
		initialised = true;
		AtomicBoolean stop = new AtomicBoolean(false);
		Thread active = new ActiveThread(node, stop);
		active.start();
		Thread mailboxUpdater = new UpdateMailboxThread(stop);
		mailboxUpdater.start();
		Thread passive = new PassiveThread(node, stop);
		passive.start();
		Thread.sleep(tps);
		stop.set(true);
	}

	/**
	 * From Gossip-Based Peer Sampling paper (2007) : Returns a peer address if the
	 * group contains more than one node. The returned address is a sample drawn
	 * from the group. Ideally, this sample should be an unbiased random sample.
	 */
	public Address getPeer() {
		if (!initialised) {
			return null;
		}
		return node.getPeer();
	}

}

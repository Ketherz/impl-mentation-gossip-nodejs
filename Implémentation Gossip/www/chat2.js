$(function(){
  function parcourtab(tab){
    cur = "";
    for(i=0; i<tab.length; i++){
       cur = cur + "," + tab[i];
    }
    return cur;
  }

//création de la zone d'identification de l'utilisateur

  var div_identification = $('<div/>').css({
          position : 'absolute',
          left:50, top:50,
          width : 300, height:80,
          border : '2px solid black'});
  div_identification.append($("<span/>").text("votre nom?"));

  var input_name = $("<input type = 'text' />").css('margin', 5);
  div_identification.append(input_name);

  div_identification.append($("<button/>").text("valider").click(function(){
    var name=input_name.val();
    div_identification.remove();
    console.log("nom --> "+name);
    startChat(name);
  }));

  var list_user_connected =[];

  $("body").append(div_identification);

//fonction de démarage du chat, elle apparait suite à la validation du pseudo

  function startChat(name){
    var io_client = io();

//On prévien quand le serveur est en ligne

    io_client.on("connect", function(){
      console.log("Connecté au serveur");
      io_client.emit("addUser", name);


//on crée et ajoute la zone de chat

      var chat_console = $("<div/>").css({
      position : "absolute",
      left : 50, top :50,
      width : 500,height:600,
      border : "2px solid green",
      padding : 10,
      overflow : 'auto',
    });
  $("body").append(chat_console);

//On crée une boite de dialogue aillant pour but d'envoyer des messages

      var chat_zone = $("<input type='text' />").css({margin : 5,
      width : 500,});
      $("body").append(chat_zone);


      $("body").append($("<button/>").text("envoyer").click(function(){
        var message=chat_zone.val();
        console.log("le message suivant a été envoyé --> "+message);
        colis = [name, message];
        console.log(name);
        console.log(colis);
        io_client.emit("Message_entrant",colis);
      }));


//On récupére le message de bonjour du serveur et on le display dans la console

        io_client.on("Hello_world", function(Hello_world){
          console.log(Hello_world);
        });

//On récupére la liste des utilisateurs connectés

        io_client.on("list_user",function(list_user){
          console.log(list_user);
          list_user_connected = list_user;
        });

//quand un utilisateur se connecte on écrit son nom
  io_client.on("nom_co", function(name){
    chat_console.append($("<div><i>L'utilisateur " + name+" vien de se connecter</i></div>"));


//a chaque connection on affiche les personnes présentes dans la salle
    if(list_user_connected.length>1){
      chat_console.append($("<div><i>Les utilisateur " + parcourtab(list_user_connected) +" sont actuellement sur scene</i></div>"));
    }
  });

//a chaque déconnexion on notifie les utilisateurs
  io_client.on("nom_deco",function(name){
    chat_console.append($("<div><i>L'utilisateur" + name +" vien de se déconnecter</i></div"));
  });

//à chaque fois qu'on reçois un message on l'écrit dans la zone de chat
  io_client.on("Message_sortant",function(colis){
    console.log("Nouveau message de "+ colis[0] + " : " + colis[1]);
    chat_console.append($("<div><i>"+colis[0]+" : "+colis[1]+"</i></div"));

  });

//On prévien quand le serveur est hors ligne

      io_client.on("disconnect", function(){
        console.log("Connection au serveur perdue");

      });
    });
  }





});
















/* end of file */

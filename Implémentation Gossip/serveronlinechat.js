//récupération de l'adresse ip (npm install ip)
var ip = require("ip");
console.dir ( "Adresse ip du serveur :" + ip.address() );

//création des différentes varables du serveur
var tab_Ip_Voisins = [];
var socket_Table = [];
var id_message_reçu = [];
var table_message_en_attente = [];
var table_Htag = [];



//récupération table de Htag et table IP

console.log("\nremplissage table de Htag et voisins")

process.argv.forEach(function(val, index, array){
  if(val.indexOf('#')!=-1 && index > 1){
    table_Htag.push(val);
  }
  else if(index > 1){
    tab_Ip_Voisins.push(val);
  }
});

console.log("\nla table des htag contient :"  + table_Htag );
console.log("\nla table des voisins contient : " + tab_Ip_Voisins);


//fonction de nettoyage de tableau

function removeInArray(tab, val){
  var pos = tab.indexOf(val);
  if(pos!= -1){
    tab.splice(pos,1);
  }
}


//Partie serveur à utiliser?


var url = require('url');
var http= require('http');
var fs = require('fs');

var server = http.createServer(function(req,res){
  var parsed_url = url.parse(req.url);
  var query_file = parsed_url.pathname;



  if(query_file == '/'){
    query_file = "/chat.html";
  }
  query_file="./www"+query_file;

  fs.stat(query_file, function(err, stat){
    if(err){
        console.log("Erreur : "+err); res.end();
      }
      else{
        if(stat.isDirectory()){
          console.log("l'élément indiqué est un dossier !");
          res.end();
        }else{
          fs.readFile(query_file, function(err, data){
            if(err){
              console.log("Erreur : "+err); res.end();}
              else{
                res.write(data);
                res.end();
              }
          });
        }
      }
    });
});

var io_server = require('socket.io')(server);
var list_server = [];
var list_user = [];

//conversation avec le client

//code prévenant de la connexion et déconnexion d'un client on envois aussi un message au client afin de lui dire bonjour

io_server.on("connection", function(socket_client){
  io_server.emit("Hello_world", "Hello_world");


//ajout du client à un tableau qu'on va envoyer au client
  socket_client.on("addUser", function(name){
    console.log("connection d'un nouveau client de nom : " + name);
    list_user.push(name);
    console.log(list_user);
    io_server.emit("list_user", list_user);
    io_server.emit("nom_co", name);




    //on programme la fonctionnalitée d'échange de messages
    socket_client.on("Message_entrant",function(colis){
      console.log("Messages echangés " + colis[0] + " : " + colis[1]);
      idmess = Math.random();
      table_message_en_attente.push({"id" :  idmess, "colis" : colis});
      id_message_reçu.push(idmess);
      io_server.emit("Message_sortant", colis);
    });


//suppression de l'utilisateur déconnecté du tableau on renvois le tableau
    socket_client.on("disconnect", function(){
      console.log("Deconnexion de " + name);
      removeInArray(list_user,name);
      console.log(list_user);
      io_server.emit("list_user", list_user);
      io_server.emit("nom_deco",name);
    });



  });
});



      //----------Communication inter serveur-----------//




      //client
      for (i=0; i < tab_Ip_Voisins.length; i++){


          io = require('socket.io-client');
          socket_Table[i] = io("http://"+tab_Ip_Voisins[i]+":3000");

          socket_Table[i].on('connect', function () {
              console.log("client connecté");


              socket_test = this;

            /*
              this.on('demande_voisin',function(data){

                 //Todo : récupérer l'ip de X
        	   	    console.log("envois des voisins à : IPx");
                  this.emit('envois_voisins', tab_Ip_Voisins);
                }

              );

              this.on('envois_voisins', function(data){

                tab_Ip_Voisins = data;
              });
              */



              //relais du message reçu

              this.on('relais_message', function(message){

                if(id_message_reçu.indexOf(message.id) != -1){
                    console.log("déja reçu");
                }

                else{

                  //ici on met la fonction d'envois des messages aux clients web
                    io_server.emit("Message_sortant", message.colis);

                    id_message_reçu.push(message.id);
                    io_server.emit("envois_message", message.colis);
                    this.emit('envois_message', message);
                    console.log(message);
                }
              });



              setInterval(function () {
                  socket_test.emit('mise_a_jour_htag', table_Htag);
              }, 5000);


            });




          }

          //fin client



      //serveur
      var io = require('socket.io').listen(3000);
      io.on('connection', function (socket) {
          socket.on('serverEvent', function (data) {
              console.log('new message from client:', data);
          });


          //fonction de demande des voisins

          socket.on('envois_voisins',function(voisins){
            console.log("table des voisins reçu");
            console.log(voisins);
          });

          //fonction de reception de message

          socket.on('envois_message', function(message){

            if(id_message_reçu.indexOf(message.id) != -1){
                console.log("déja reçu");
            }

            else{
                id_message_reçu.push(message.id);
                io_server.emit("Message_sortant", message.colis);
                //// TODO: Envoyer le message vers la salle de chat
                socket.emit("relais_message", message);
            }

          });


          //fonctions de relais de message

          setInterval(function sendmessage() {
              if(table_message_en_attente[0]!=undefined){
                console.log("sended");
                socket.emit('relais_message', table_message_en_attente[0]);
                table_message_en_attente.splice(0,1);
                sendmessage();
              }
              else{
                console.log("vide");
              }

          }, 5000);


          //fonction de mise à jour des Htag

          socket.on('mise_a_jour_htag', function(htagtablerecup){
            for(i=0; i<htagtablerecup.length; i++){
              if(table_Htag.indexOf(htagtablerecup[i]) == -1){
                table_Htag.push(htagtablerecup[i]);
                console.log("ajout de :" + htagtablerecup[i]);
                //dans le cas de l'ajout du hashtag, on peut imaginer un dictionnaire qui résupére le Htag et associe l'ip de celui qui posséde le Htag
              }
            }
          });

      });

      //fin serveur



server.listen(2000);
console.log("\n serveur pret et paré à fonctionner");


























/* end of file*/

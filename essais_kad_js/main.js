const KadDHT = require('libp2p-kad-dht')
const dht = new KadDHT({
      dialer: {
        connectToPeer
      },
      registrar: createMockRegistrar(regRecord),
      peerStore,
      peerInfo: p,
      validators: {
        v: {
          func () {
            return Promise.resolve(true)
          },
          sign: false
        },
        v2: {
          func () {
            return Promise.resolve(true)
          },
          sign: false
        }
      },
      selectors: {
        v: () => 0
      },
      ...options
    })

    dht.start()
